# @author: Sasank Venkata Vishnubhatla
# @version: 1.0

__name__ = '__EllipticalCurve__'

class EllipticalCurve(object):
    def __init__(self, a, b):
        # must be in Weierstrass form
        self.a = a
        self.b = b
        self.discriminant = int(-16 * (4 * a*a*a + 27 * b*b))
        if not self.isSmooth():
            raise Exception("The curve %s is not smooth!" % self)
    # Check to see if the Elliptical Curve is smooth
    def isSmooth(self):
        return self.discriminant != 0
    # to string method
    def __str__(self):
        return "y^2 = x^3 + %fx + %f" % (int(self.a), int(self.b))
    # Check to see if two curves are equal
    def __eq__(self, other):
        return (self.a, self.b) == (other.a, other.b)
    # Find slope of a curve at point (x,y)
    def slope(self, x, y):
        return (3 * x*x + self.a) / (2 * y)
    # Solve for y^2-value on curve
    def solveForY2(self, x):
        return int(x*x*x + self.a*x + self.b)