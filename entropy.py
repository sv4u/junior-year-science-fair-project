# @author: Sasank Venkata Vishnubhatla
# @version: 1.0

import math
from collections import Counter

# Returns entropy of string
#
# @param string     data string
# @return float     entropy of data string
def entropy(s):
    p, lns = Counter(s), float(len(s))
    return -sum(count/lns * math.log(count/lns, 2) for count in p.values())