# @author: Sasank Venkata Vishnubhatla
# @version: 1.0

import hashlib, numpy as np, ellipticalcurve as ecc

__name__ = "__hashes__"
# Returns new elliptical curve hash from a normalized image
#
# @param list       Normalized image with iris as a list
# @returns int      Elliptical curve hash
def ech(image):
    message = image
    nums = [ord(c) for c in message]
    length = len(nums)
    xor = range(1, length)
    t, l_nums, l_t = [0] * length, [0] * length, [0] * length
    for i in range(0, length - 1):
        t[i] = nums[i] ^ xor[i]
        l_nums[i] = nums[i] ^ length
        l_t[i] = t[i] ^ length
    s_nums, s_t = sum(nums), sum(t) # find sums of nums and t
    s_l_nums, s_l_t = sum(l_nums), sum(l_t) # find sums of nums xor l and nt xor l
    eq1, eq2 = ecc.EllipticalCurve(s_nums, s_t), ecc.EllipticalCurve(s_l_nums, s_l_t) # generate equations
    y2_s_nums, y2_s_t = eq1.solveForY2(s_nums), eq1.solveForY2(s_t) # find y^2-values
    y2_s_l_nums, y2_s_l_t = eq2.solveForY2(s_l_nums), eq2.solveForY2(s_l_t) # find y^2-values
    d_dx_y2_s_nums, d_dx_y2_s_t = eq1.slope(s_nums, y2_s_nums ** (1/2)), eq1.slope(s_t, y2_s_t ** (1/2)) # find slopes
    d_dx_y2_s_l_nums, d_dx_y2_s_l_t = eq2.slope(s_l_nums, y2_s_l_nums ** (1/2)), eq1.slope(s_l_t, y2_s_l_t ** (1/2)) # find slopes
    h_s = d_dx_y2_s_t % d_dx_y2_s_nums # find part one of hash
    h_s_l = d_dx_y2_s_l_t % d_dx_y2_s_l_nums # find part two of hash
    hash = h_s * h_s_l * (h_s ^ h_s_l) # define full hash
    ech = hex(hash)
    return ech

# Returns MD5 hash from a normalized image
#
# @param list       Normalized image with iris as a list
# @returns int      MD5 hash value
def md5(list):
    message = list
    m = hashlib.md5()
    m.update(list)
    md5 = m.hexdigest()
    return md5

# Returns SHA-1 hash from a normalized image
#
# @param list       Normalized image with iris as a list
# @returns int      SHA-1 hash
def sha1(list):
    message = list
    sha1 = hashlib.sha1(message).hexdigest()
    return sha1
